## Motivation

I've lived in Hungary / Budapest, relocated to work in Luxembourg, and possibly live in Germany, near Trier, or somewhere else. In the meantime,
there was some confusing things, but thanks to my colleges, I got many helpful information which you may find useful, so I've decided to share it

## If you come by plane

- sometimes, a 2 way ticket is less expensive than one way
- If your plane is late and you miss the connection because of that, Lufthansa will rebook you to the next plane without problems.
  they even gave me a 10EUR vocheur
- It's much cheaper to fly to Brussels and then go to LUX by transport, but takes ~10+ hour and requires more planning.

## Salary

After you know your gross salary, you can calculate the taxation in https://www.calculatrice.lu/calculator --- but !

- Marriage doesn't matter until your wife lives in the same address as you, from where you come to work.
- same thing with child, same household matters

* [Non resident taxplayer registration](http://www.impotsdirects.public.lu/fr/formulaires/fiches_d_impot.html) - 164 NR (german / french version)
* [Ask for the social security card](http://www.ccss.lu/certificats/assures/carte-europeenne-de-securite-sociale/) you will need it, especially if you travel abroad (valid in EU as well)

## Temporary accomodation, while looking for a rent

- cheap hotel: http://www.zewener-hof.de/ - booking.com can also be a good solution.

## Rent

- Rents in germany (near LUX) used to live ~1-5 days, so you should only ask for appointment a few days before you can actually go there
- They used to ask 2-3 month of cold rent as a security deposit, and the first month's hot rent. Hot rent = include stuff like heating, cold rent = just the rent.
- some links to look for a rent: https://www.immowelt.de/ https://www.athome.de/ https://www.immotop.lu/ https://www.immobilienscout24.de/ 
- see https://www.check24.de/ to see what kind of internet connection / provider is available at the address
- good and cheap furniture shop. https://www.poco.de - watch for actions, 20-40% difference is not rare.

## Phone

- one of the best provider is lebara, you can order the sim online, and then activate it. It may take time as their support is not very fast.

## Bank account

- There are a lots of options, for example, N26, which is online, you don't ever have to go to the bank. You have to have an address in germany or other "supported" countries. Ask for
  your fellow colleges to send you an invite, they receive money from it.

## Spending money

- If you have a 3D-mastercard, it's cheaper to use it to buy stuff, than buying EUR
- they don't like 500EUR, it's very hard to change it, so try to avoid

## Transportation

- Bus ticket prices: https://www.mobiliteit.lu/se-deplacer/titres-de-transport/tickets-transfrontaliers
- Regions per city https://www.mobiliteit.lu/se-deplacer/titres-de-transport/tickets-transfrontaliers/regiozone - it has multiple pages (pagination) and a search function in the top of the table
- These bus lines are operated by LUX, so german holidays doesn't apply.

